angular.module('starter.services', ['ngResource'])

  .factory('CategoriasResource',function($resource){

    return $resource('http://valentinaecu.com/api_valentina/public/all_categorias');
  })

  .factory('TodoResource',function($resource){

            return $resource('http://valentinaecu.com/api_valentina/public/todo_producto');
  })

	.factory('UserService',function($rootScope,$http,$localStorage){
    return {

      getData : function(){
        return {
                usuario_id : $localStorage.v_id,
                nombre: $localStorage.v_nombre,
                email: $localStorage.v_email,
                telefono1 : $localStorage.v_telefono1,
                telefono2 : $localStorage.v_telefono2,
                direccion : $localStorage.v_direccion,
                numero_afiliado : $localStorage.v_numero_afiliado,
                afiliado_id : $localStorage.v_afiliado_id


                }
        },

      setData : function(usuario){

                 $localStorage.v_email = usuario.email ;
                 $localStorage.v_nombre = usuario.name;
                 $localStorage.v_id= usuario.id;
                 $localStorage.v_numero_afiliado= usuario.n_afiliado;
                 $localStorage.v_telefono = usuario.afiliado.telefono;
                 $localStorage.v_direccion = usuario.afiliado.direccion;
                 $localStorage.v_telefono1 = usuario.afiliado.telefono1;
                 $localStorage.v_telefono2 = usuario.afiliado.telefono2;
                 $localStorage.v_afiliado_id = usuario.afiliado.id;

      },
      check : function(){

              if ($localStorage.v_id == undefined){

                  return false;

              }else{

                    return true;
              }

      },
       checkTutorial : function(){

              if ($localStorage.v_tutorial == undefined){

                  return false;

              }else{

                    return true;
              }

      }
    };



    });
