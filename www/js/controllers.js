angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$ionicSlideBoxDelegate,UserService,$http,$ionicLoading,$state,$cordovaToast,$localStorage,CategoriasResource,TodoResource) {

  $scope.cantidad_carrito = 0;
  $scope.muestraLogin = !UserService.check();
  $scope.muestraSession = UserService.check();
  $scope.datosAfiliado = UserService.getData();
  $scope.productosTodos = TodoResource.query();
  $scope.todosBusqueda = '';
  console.log($scope.productosTodos);

  $scope.producto;
  $scope.productoPedido ={};
  $scope.pedido = [];
  $scope.total = 0;
  $scope.categorias = CategoriasResource.query();

  // Form data for the login modal
  $scope.loginData = {};
  $scope.modal = {};
  $scope.resultadoCategoria = {};
  $scope.busquedaResultado = '';
  $scope.showToast = function(message, duration, location) {
        $cordovaToast.show(message, duration, location).then(function(success) {
            console.log("Mensaje mostrado");
        }, function (error) {
            console.log("el mensaje no pudo ser mostrado " + error);
        });
    }


  // Create the login modal that we will use later

  $scope.showLoading = function() {
    $ionicLoading.show({
      template: '<ion-spinner icon="spiral"></ion-spinner>'
    });
  };

  $scope.hideLoading = function(){
        $ionicLoading.hide();
  };



  // Triggered in the login modal to close it
  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
     $ionicModal.fromTemplateUrl('templates/social/login.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();

    });
  };

   // Open cantidad modal
  $scope.cantidadModal = function() {
     $ionicModal.fromTemplateUrl('templates/social/cantidad.html', {
      scope: $scope,
      animation: 'none'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();

    });
  };

   $scope.muestraBusqueda = function() {
     $ionicModal.fromTemplateUrl('templates/social/busqueda.html', {
      scope: $scope,
      animation: 'none'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();

    });
  };




  $scope.cerrarSesion = function(){

       $localStorage.$reset();
       $scope.muestraSession = UserService.check();
       $scope.muestraLogin = !UserService.check();
       $scope.datosCliente = UserService.getData();
       $scope.datosAfiliado = {};
       $scope.showToast('Ha cerrado sesion', 'long', 'bottom');

  }
   $scope.mostrarLoading = function(){

    $ionicLoading.show({
      template: 'Iniciando Sesion...'
    }).then(function(){
       console.log("Iniciando Sesion");
    });

  }
   $scope.hide = function(){
    $ionicLoading.hide().then(function(){
    });
  };

  $scope.iniciarSesion = function(){

     $scope.mostrarLoading();
    var credentials= {n_afiliado:$scope.loginData.n_afiliado,password:$scope.loginData.password};
    var uploadUrl='http://valentinaecu.com/api_valentina/public/acceso_cliente';
      $scope.formatLogin(uploadUrl,credentials);



  }
  $scope.formatLogin = function(uploadUrl, data){
    var fd = new FormData();
    for(var key in data)
      fd.append(key, data[key]);
    var result = $http.post(uploadUrl, fd, {
      transformRequest: angular.indentity,
      headers: { 'Content-Type': undefined }
    });
               result.success(function (data) {




                            UserService.setData(data);
                            $scope.logout = UserService.check();
                            $scope.muestraLogin = !UserService.check();
                            $scope.muestraSession = UserService.getData();
                            $scope.datosAfiliado = UserService.getData();
                            $scope.hide();
                            $scope.closeModal();
                             $scope.showToast('Ha iniciado sesion', 'long', 'bottom');




                  }).error(function(data){
                      $scope.hide();
                     $scope.showToast('Correo o Contrasena Invalido', 'long', 'bottom');



                  });
  }

  $scope.selectCantidad = function(producto,index){

        $scope.producto  = producto;
        $scope.cantidadModal();

  }
  $scope.hacerAlgo = function(){
      console.log('hola');
  }

  $scope.BuscarItem=function(id){
    var detalle={};

    if (!$scope.pedido.length==0) {
      for (var i = 0; i < $scope.pedido.length; i++) {

        if ($scope.pedido[i].producto_id == id) {
          detalle=$scope.pedido[i];
        }
      }
    }
    return detalle;
  }

  $scope.borrarItem = function(index){

    $scope.pedido.splice(index,1);
    $scope.cantidad_carrito = $scope.pedido.length;
    $scope.showToast('Item Eliminado','long','bottom');
  }

  $scope.editarItem = function(producto){

        console.log(index);
        console.log($scope.pedido);
        $scope.producto  = producto;
        $scope.cantidadModal();

  }

  $scope.agregarCarrito = function(){
        var precio = $scope.productoPedido.cantidad * $scope.producto.precioUnitarioVenta;
        $scope.total = $scope.total + precio;

        $scope.productoPedido.cantidad = $scope.productoPedido.cantidad;
        $scope.productoPedido.precio = precio;
        $scope.productoPedido.producto_id = $scope.producto.id;
        $scope.productoPedido.descripcion = $scope.producto.descripcion;
        $scope.productoPedido.codigo = $scope.producto.codigo;

        $scope.pedido.push($scope.productoPedido);


          $scope.cantidad_carrito = $scope.pedido.length;
          $scope.closeModal();
          $scope.eliminarProducto($scope.producto.id);
          $scope.productoPedido = {};
          $scope.showToast('Articulo agregado a carrito', 'long', 'bottom');


  }

  $scope.eliminarProducto=function(id){
    for (var i = 0; i < $scope.resultadoCategoria.length; i++) {
      if ($scope.resultadoCategoria[i].id==id) {
        $scope.resultadoCategoria.splice(i, 1);
      }
    }
  }


  $scope.listadoCategoria = function(categoria){

      $scope.showLoading();
        var resultado = $http({
                        url: 'http://valentinaecu.com/api_valentina/public/get_categoria/'+categoria,
                        method: "GET",
                        headers: {

                          'Content-Type': undefined
                        }
                    });
        resultado.then(function(resultado){

            $scope.resultadoCategoria = resultado.data;
            $scope.hideLoading();

            $state.go('app.resultado');
        });
  }
  $scope.listadoCategoria2 = function(categoria){

      $scope.showLoading();
        var resultado = $http({
                        url: 'http://valentinaecu.com/api_valentina/public/get_categoria/'+categoria,
                        method: "GET",
                        headers: {

                          'Content-Type': undefined
                        }
                    });
        resultado.then(function(resultado){
          $scope.closeModal();
            $scope.resultadoCategoria = resultado.data;
            $scope.hideLoading();

            $state.go('app.resultado');
        });
  }

  $scope.enviarPedido = function(){

    if (UserService.check() == false){


        $scope.login();
        $scope.showToast('Debe iniciar sesion', 'long', 'bottom');

    }else{

       $scope.data= {};

      $scope.data= {

            numero_afiliado : $scope.datosAfiliado.numero_afiliado,
            cliente_id : $scope.datosAfiliado.afiliado_id,
            total : $scope.total,
            pedido : angular.toJson($scope.pedido),


      };

      $scope.format('http://valentinaecu.com/api_valentina/public/hacerPedido',$scope.data);


    }




  }

  $scope.format = function(uploadUrl, data){
    $scope.showLoading();
    var fd = new FormData();
    for(var key in data)
      fd.append(key, data[key]);
    var result = $http.post(uploadUrl, fd, {
      transformRequest: angular.indentity,
      headers: { 'Content-Type': undefined }
    });
               result.success(function (data) {

                     $scope.hideLoading();
                     $scope.pedido = [];
                     $scope.cantidad_carrito = 0;
                     $scope.total = 0;
                     $state.go('app.principal');
                      $scope.showToast('Pedido enviado con Exito', 'long', 'bottom');



                  }).error(function(data){


                     console.log(data);

                  });
  }


})
.controller('DashCtrl', function($scope, $state, $http , $ionicSlideBoxDelegate,$stateParams) {
  $scope.categorias = {};
  $scope.items = $stateParams.producto;
  $scope.navSlide = function(index) {
        $ionicSlideBoxDelegate.slide(index, 500);
      }
})

.controller('ProductosCtrl', function($scope,constantesSistema, $rootScope,$state, $http , $ionicSlideBoxDelegate,$stateParams,$ionicModal,$ionicLoading) {
  $scope.categorias = {};
  $scope.producto_actual = {};
  $rootScope.cantidad=0;
  $rootScope.total=0;
  $rootScope.detalles;
  $scope.TotalPedido=$rootScope.TotalPedido;
  $scope.constantesSistema=constantesSistema;
  $rootScope.mostrarLoading('CARGANDO PRODUCTOS');
  $http.get(constantesSistema.url_recursos + 'get_items').then(function(res){
    $scope.categorias = res.data;
    $ionicLoading.hide();
  },function(error){$rootScope.showAlert('REVISE SU CONEXION A INTERNET O ACTIVE SU UBICACION' );});
})

.controller('PedidoCtrl', function($scope ,$rootScope, $http, $state,constantesSistema,$ionicLoading) {
  $rootScope.detalles;
  $scope.longitud = null;
  $scope.latitud=null;
  $scope.constantesSistema=constantesSistema;
  console.log($rootScope.detalles);
  navigator.geolocation.getCurrentPosition(function(position){
    $scope.longitud = position.coords.latitude;
    $scope.latitud=position.coords.longitude;
  });
  $scope.EnviarPedido=function() {
    if ($rootScope.detalles.length>0 && this.direccion_entrega.length>0) {
      $scope.direccion_entrega=this.direccion_entrega;
      $scope.data={};
      $scope.data={
        longitud: $scope.longitud,
        latitud: $scope.latitud,
        total_pedido:$rootScope.TotalPedido,
        cliente_id:$scope.constantesSistema.cliente_id,
        direccion_entrega: $scope.direccion_entrega,
        total: $rootScope.TotalPedido,
        detalles: $rootScope.detalles
      };
      console.log(angular.toJson($scope.data));
      $rootScope.mostrarLoading("SE ESTA ENVIANDO SU PEDIDO");
      var conAjax = $http.post( constantesSistema.url_recursos+ "pedido" ,angular.toJson($scope.data)).then(function(res){
        $rootScope.detalles=[];
        $rootScope.TotalPedido=0;
        this.direccion_entrega=null;
        $ionicLoading.hide();
        $rootScope.showAlert("Su pedido esta siendo procesado");
        $state.go('tab.productos');
      },function(error){
        $ionicLoading.hide();
        $rootScope.showAlert("Revise su conexion el pedido no se a podido procesar");
        $state.go('tab.pedido');
      });


    }else{
      $rootScope.showAlert("REVISE SU SOLICITUD");
      $state.go('tab.productos');
    }

  }
})

.controller('editPedidoCtrl', function($scope, $stateParams, $rootScope,constantesSistema,$state) {
$scope.detalle={};
  $scope.detalle = $rootScope.BuscarItem($stateParams.itemId);
  $scope.detalleEditado = {};
  $scope.cantidad=$scope.detalle.cantidad;
  $scope.constantesSistema=constantesSistema;
  $scope.ActualizarPedido =function(){
    $scope.detalleEditado.item=$scope.detalle.item;
    $scope.detalleEditado.cantidad=this.cantidad;
    $scope.detalleEditado.costo_total=this.total;
    for (var i = 0; i < $rootScope.detalles.length; i++) {
      if ($rootScope.detalles[i].item.id==$scope.detalleEditado.item.id) {
        $rootScope.detalles[i].cantidad=this.cantidad;
        $rootScope.detalles[i].costo_total=this.total;
        console.log("Actulizado correctamente");
      }
    }
    $rootScope.CalcularTotalPedido();
    $rootScope.showAlert("PEDIDO ACTUALIZADO CORRECTAMENTE");
    $state.go('tab.pedido');
    // $rootScope.CalcularTotalPedido();
    // $state.go('tab.productos');
  }

})
.controller('DetalleCtl', function($scope, $stateParams,$rootScope,$state,$http,constantesSistema,$ionicLoading) {
  var id=$stateParams.id;
  $scope.producto_actual={};
  $scope.cantidad=0;
  $scope.total=0;
  $rootScope.CalcularTotal=function(){
      $scope.total=this.cantidad*$scope.producto_actual.precio;
      $scope.total=Math.round(this.total * 100) / 100 ;
  }
  $scope.BuscarProducto=function(id){
    $rootScope.mostrarLoading('PROCESANDO');
    $http.get(constantesSistema.url_recursos + 'get_items').then(function(res){
      $scope.categorias = res.data;
      $scope.categorias.forEach(function(respuesta){
        respuesta.items.forEach(function(auxRes){
          if (auxRes.id==id) {
            $scope.producto_actual= auxRes;
            $ionicLoading.hide();
          }
        })

      });

    })
  }
  $scope.BuscarProducto(id);
  $scope.total=this.cantidad*$scope.producto_actual.precio;
  $scope.constantesSistema=constantesSistema;
  $scope.AgregarPedido =function(){
    $scope.detalle={};
    $scope.detalle.item=$scope.producto_actual;
    $scope.detalle.cantidad=this.cantidad;
    $scope.detalle.costo_total=this.total;
    $rootScope.detalles.push($scope.detalle);
    $rootScope.CalcularTotalPedido();
    $state.go('tab.productos');
  }

})

.controller('RegistroCtl', function($scope,$state,$http,$rootScope,constantesSistema) {
$scope.cliente={};
  $scope.RegistrarCliente=function(){
    console.log('registrandose');
    console.log($scope.cliente);
    var conAjax = $http.post( constantesSistema.url_recursos+ "cliente" ,angular.toJson($scope.cliente))
    .then(function(res){
      $scope.cliente={};
      $rootScope.showAlert("Cliente registrado correctamente");
      $state.go('tab.account');
    },function(error){
      $rootScope.showAlert("Revise sus datos");
    });
  }

})
.controller('AccountCtrl', function($scope,$state,$localStorage,$http,constantesSistema,$ionicLoading,$rootScope) {
  $scope.constantesSistema=constantesSistema;
  $scope.user={};
$scope.IniciarSesion=function(){
  console.log(angular.toJson($scope.user));
  $http.post( constantesSistema.url_recursos+ "acceso_cliente" ,angular.toJson($scope.user)).then(function(res){

    $rootScope.showAlert("BIENVENIDO GRACIAS POR REGISTRARSE");
    $state.go('tab.productos');
  },function(error){
    $ionicLoading.hide();
    $rootScope.showAlert("Revise su conexion el pedido no se a podido procesar");
    $state.go('tab.pedido');
  });
}});
